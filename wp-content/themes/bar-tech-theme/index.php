<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 */

get_header(); ?>

<div class="hero">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/21457819.jpg" alt="hero image">
</div>

<div class="grid-container">

	<div class="content">

		<header class="article-header">
			<h1 class="page-title">Aktualności</h1>
		</header> <!-- end article header -->

		<div class="inner-content grid-x grid-margin-x">

		    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<!-- To see additional archive styles, visit the /parts directory -->
				<?php get_template_part( 'parts/loop', 'archive' ); ?>

			<?php endwhile; ?>

				<?php joints_page_navi(); ?>

			<?php else : ?>

				<?php get_template_part( 'parts/content', 'missing' ); ?>

			<?php endif; ?>

		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

</div>

<?php get_footer(); ?>
