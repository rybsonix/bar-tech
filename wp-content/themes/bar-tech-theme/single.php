<?php
/**
 * The template for displaying all single posts and attachments
 */

get_header(); ?>

<div class="hero">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/21457819.png" alt="hero image">
</div>

<div class="grid-container grid-container--small">

	<div class="content">

		<div class="inner-content grid-x grid-margin-x grid-padding-x">

			<main class="main small-12 cell" role="main">

			    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			    	<?php get_template_part( 'parts/loop', 'single' ); ?>

			    <?php endwhile; else : ?>

			   		<?php get_template_part( 'parts/content', 'missing' ); ?>

			    <?php endif; ?>

			</main> <!-- end #main -->

		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

</div>

<?php get_footer(); ?>
