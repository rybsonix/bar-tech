<?php
/**
 * Template part for displaying posts
 *
 * Used for single, index, archive, search.
 */
?>
<div class="cell small-12 medium-4">

	<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">

		<header class="article-header">
			<div class="archive-img">
				<a href="<?php the_permalink() ?>"><?php the_post_thumbnail('full'); ?></a>
			</div><h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
			<?php get_template_part( 'parts/content', 'byline' ); ?>
		</header> <!-- end article header -->

		<section class="entry-content" itemprop="text">
			<?php fl_excerpt($post->ID, 'fl_archive'); ?>
		</section> <!-- end article section -->

		<footer class="article-footer">
	    	<p class="tags"><?php the_tags('<span class="tags-title">' . __('Tags:', 'jointswp') . '</span> ', ', ', ''); ?></p>
		</footer> <!-- end article footer -->

	</article> <!-- end article -->

</div>
